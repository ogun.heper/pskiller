package main

import (
	"strings"
	"time"
	"os"
	log "github.com/sirupsen/logrus"
	"github.com/gobuffalo/packr"
	"github.com/shirou/gopsutil/process"
)

// TODO: add tests

// TODO: read the following as command line parameters
// TODO: add different killing strategies (disabled, interactive, automatic)
const killingEnabled = false
const protectYourself = true
const delayBetweenCycles = 1000
const logLevel = log.InfoLevel

// TODO: optimize the following array, possibly convert to a set like data structure
var targetStrings = make([]string, 0)
var myPid = os.Getpid()

type incarnationInfo struct {
	createTime int64
	cycleCount uint64
}

var incarnationCache = make(map[int32]*incarnationInfo)

func main() {
	log.SetOutput(os.Stdout)
	log.SetFormatter(&log.TextFormatter{DisableColors: true, FullTimestamp: true, TimestampFormat: "2006-01-02T15:04:05.000Z07:00"})
	log.SetLevel(logLevel)

	initializeTargetList()
	log.Infoln("Target list contains", len(targetStrings), "elements.")

	cycleNum := uint64(0)

	for {
		cycleNum++
		log.Infoln("Running cycle", cycleNum)
		killTargetProcesses()
		time.Sleep(delayBetweenCycles * time.Millisecond)
	}
}
func initializeTargetList() {
	box := packr.NewBox("./resources/targets")
	for _, fileName := range box.List() {
		if strings.HasPrefix(fileName, "target") {
			log.Infoln("Processing", fileName)
			for _, target := range strings.Split(box.String(fileName), "\n") {
				if len(target) > 0 && !strings.HasPrefix(target, "#") {
					log.Infoln("Adding", target, "to target list")
					targetStrings = append(targetStrings, target)
				}
			}
		}
	}
}

func killTargetProcesses() {
	pids, _ := process.Pids()

	for _, pid := range pids {
		process, _ := process.NewProcess(pid)

		if shouldTerminateProcess(process) {
			terminateProcess(process)
		}
	}
}

func terminateProcess(process *process.Process) {
	cmdline, _ := process.Cmdline()
	log.Infoln("Killing", cmdline)

	if killingEnabled {
		if protectYourself && myPid == int(process.Pid) {
			log.Warnln("Sorry, I can not kill myself...")
		} else {
			err := process.Kill()
			if err != nil {
				log.Errorln("Error while killing", err)
			}
		}
	}
}

func shouldTerminateProcess(process *process.Process) bool {
	if isProcessNewOrReincarnated(process) {
		// process is a new or a reincarnated process, we need to check the cmdline

		if shouldTerminate(process) {
			// this process should be terminated
			return true
		}
	}

	// process is either an already existing process, or a new one that should not be terminated, just update the stats
	updateIncarnationCache(process)
	log.Debugln("Skipping process", process.Pid, "cycleCount:", incarnationCache[process.Pid].cycleCount)
	return false
}

func isProcessNewOrReincarnated(process *process.Process) bool {
	pid := process.Pid

	incarnationInfo, exists := incarnationCache[pid]

	if exists {
		processCreateTime, _ := process.CreateTime()
		if processCreateTime != incarnationInfo.createTime {
			// this is a reincarnated process, remove the obsolete process entry from cache
			removeProcessFromIncarnationCache(process)
			return true
		}

		// this is the same incarnation of the existing process
		return false
	}

	// this is a newly created process
	return true
}

func updateIncarnationCache(process *process.Process) {
	incarnationInfo, exists := incarnationCache[process.Pid]
	if exists {
		incarnationInfo.cycleCount++
	} else {
		addProcessToIncarnationCache(process)
	}
}

func addProcessToIncarnationCache(process *process.Process) {
	createTime, _ := process.CreateTime()
	incarnationCache[process.Pid] = &incarnationInfo{createTime, 1}
}

func removeProcessFromIncarnationCache(process *process.Process) {
	delete(incarnationCache, process.Pid)
}

func shouldTerminate(process *process.Process) bool {
	exe, _ := process.Exe()
	name, _ := process.Name()
	cmdline, _ := process.Cmdline()
	log.WithFields(log.Fields{"exe": exe, "name": name, "cmdline": cmdline}).Infoln("Checking to terminate")

	// TODO: optionally check for exe and name matches
	for _, target := range targetStrings {
		if strings.Contains(cmdline, target) {
			log.Warnln("Target match found in cmdline:", target)
			return true
		}
		if strings.Contains(exe, target) {
			log.Warnln("Target match found in exe:", target)
			return true
		}
		if strings.Contains(name, target) {
			log.Warnln("Target match found in name:", target)
			return true
		}
	}
	return false
}
