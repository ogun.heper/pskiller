#!/usr/bin/env bash

echo "Building for darwin"
packr build -v

echo "Building for windows"
GOARCH=amd64 GOOS=windows packr build -v
